﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string FullMorseCode;

        private void MorseDel_Click(object sender, RoutedEventArgs e)
        {
            string MT = MorseText.Text;
            if (MT != "Enter Morse Code")
            {
                if (MT.Length > 1) MT = MT.Substring(0, MT.Length - 1);
                else MT = "Enter Morse Code";

                MorseText.Text = MT;
            }
        }

        private void ResultDel_Click(object sender, RoutedEventArgs e)
        {
            string RT = ResultText.Text;
            if (RT != "Result")
            {
                if (RT.Length > 1)
                {
                    RT = RT.Substring(0, RT.Length - 1);
                    string MorseCodeToRemove = FullMorseCode.Substring(FullMorseCode.Substring(0, FullMorseCode.LastIndexOf(" ")).LastIndexOf(" ") + 1);
                    if (FullMorseCode.Length > 0) FullMorseCode = FullMorseCode.Remove(FullMorseCode.Length - MorseCodeToRemove.Length);
                }
                else RT = "Result";

                ResultText.Text = RT;
            }
        }

        private void DotButton_Click(object sender, RoutedEventArgs e)
        {
            if (MorseText.Text == "Enter Morse Code") MorseText.Text = ".";
            else MorseText.Text += ".";
        }

        private void DashButton_Click(object sender, RoutedEventArgs e)
        {
            if (MorseText.Text == "Enter Morse Code") MorseText.Text = "-";
            else MorseText.Text += "-";
        }

        private void SpaceButton_Click(object sender, RoutedEventArgs e)
        {
            if (MorseText.Text == "Enter Morse Code" && ResultText.Text != "Result")
            {
                FullMorseCode += "/ ";
                ResultText.Text += " ";
            }
            if (MorseText.Text != "Enter Morse Code")
            {
                if (ResultText.Text == "Result") ResultText.Text = "";
                switch (MorseText.Text)
                {
                    case ".-":
                        CaseSharedEvents("A");
                        break;
                    case "-...":
                        CaseSharedEvents("B");
                        break;
                    case "-.-.":
                        CaseSharedEvents("C");
                        break;
                    case "-..":
                        CaseSharedEvents("D");
                        break;
                    case ".":
                        CaseSharedEvents("E");
                        break;
                    case "..-.":
                        CaseSharedEvents("F");
                        break;
                    case "--.":
                        CaseSharedEvents("G");
                        break;
                    case "....":
                        CaseSharedEvents("H");
                        break;
                    case "..":
                        CaseSharedEvents("I");
                        break;
                    case ".---":
                        CaseSharedEvents("J");
                        break;
                    case "-.-":
                        CaseSharedEvents("K");
                        break;
                    case ".-..":
                        CaseSharedEvents("L");
                        break;
                    case "--":
                        CaseSharedEvents("M");
                        break;
                    case "-.":
                        CaseSharedEvents("N");
                        break;
                    case "---":
                        CaseSharedEvents("O");
                        break;
                    case ".--.":
                        CaseSharedEvents("P");
                        break;
                    case "--.-":
                        CaseSharedEvents("Q");
                        break;
                    case ".-.":
                        CaseSharedEvents("R");
                        break;
                    case "...":
                        CaseSharedEvents("S");
                        break;
                    case "-":
                        CaseSharedEvents("T");
                        break;
                    case "..-":
                        CaseSharedEvents("U");
                        break;
                    case "...-":
                        CaseSharedEvents("V");
                        break;
                    case ".--":
                        CaseSharedEvents("W");
                        break;
                    case "-..-":
                        CaseSharedEvents("X");
                        break;
                    case "-.--":
                        CaseSharedEvents("Y");
                        break;
                    case "--..":
                        CaseSharedEvents("Z");
                        break;
                    case ".----":
                        CaseSharedEvents("1");
                        break;
                    case "..---":
                        CaseSharedEvents("2");
                        break;
                    case "...--":
                        CaseSharedEvents("3");
                        break;
                    case "....-":
                        CaseSharedEvents("4");
                        break;
                    case ".....":
                        CaseSharedEvents("5");
                        break;
                    case "-....":
                        CaseSharedEvents("6");
                        break;
                    case "--...":
                        CaseSharedEvents("7");
                        break;
                    case "---..":
                        CaseSharedEvents("8");
                        break;
                    case "----.":
                        CaseSharedEvents("9");
                        break;
                    case "-----":
                        CaseSharedEvents("0");
                        break;
                }
            }
        }

        private void CaseSharedEvents(string m)
        {
            FullMorseCode += MorseText.Text + " ";
            MorseText.Text = "Enter Morse Code";
            ResultText.Text += m;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to clear all?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                //do nothing
            }
            else
            {
                FullMorseCode = "";
                MorseText.Text = "Enter Morse Code";
                ResultText.Text = "Result";
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (ResultText.Text == "Result")
            {
                MessageBox.Show("Nothing to save.");
            }
            else
            {
                string saveResult = FullMorseCode + " = " + ResultText.Text;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() == true)
                    File.WriteAllText(saveFileDialog.FileName + ".txt", saveResult);
            }
        }
    }
}
